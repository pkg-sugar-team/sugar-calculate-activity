Source: sugar-calculate-activity
Section: x11
Priority: optional
Maintainer: Debian Sugar Team <pkg-sugar-devel@lists.alioth.debian.org>
Uploaders:
 Jonas Smedegaard <dr@jones.dk>,
Build-Depends:
 debhelper-compat (= 12),
 dh-linktree,
 dh-sequence-python3,
 python3,
 python3-sugar3 (>= 0.118-3~),
 unzip,
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/pkg-sugar-team/sugar-calculate-activity.git
Vcs-Browser: https://salsa.debian.org/pkg-sugar-team/sugar-calculate-activity
Homepage: https://wiki.sugarlabs.org/go/Activities/Calculate
Rules-Requires-Root: no

Package: sugar-calculate-activity
Architecture: all
Depends:
 gir1.2-glib-2.0,
 gir1.2-gtk-3.0,
 gir1.2-pango-1.0,
 gir1.2-rsvg-2.0,
 gir1.2-telepathyglib-0.12,
 python3,
 python3-dbus,
 python3-gi,
 python3-sugar3,
 ${misc:Depends},
 ${python3:Depends},
Recommends:
 python3-matplotlib,
Provides:
 ${python3:Provides},
Description: Sugar Learning Platform - calculation activity
 Sugar Learning Platform promotes collaborative learning
 through Sugar Activities that encourage critical thinking,
 the heart of a quality education.
 Designed from the ground up especially for children,
 Sugar offers an alternative to traditional "office-desktop" software.
 .
 Learner applications in Sugar are called Activities.
 They are software packages that automatically save your work -
 producing specific instances of the Activity
 that can be resumed at a later time.
 Many Activities support learner collaboration,
 where multiple learners may be invited
 to join a collective Activity session.
 .
 The Calculate Activity is an infix-notation graphing calculator.
 Type an expression or select components from the toolbars,
 and press Return to evaluate it.
